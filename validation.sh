#!/bin/bash

readarray -t prefixarray < $1
prefix=${2}

echo "prefix you passed:$prefix"

for item in ${prefixarray[@]}; do
  
   if [[ "${item}" =~ $prefix ]] 
     then
     echo "$prefix is already exist in the environment"
     exit 1
   fi
done